package workshop.exercices.movies;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;

import workshop.Utils;
import workshop.movies.utils.MovieSimilarity;


/**
 * Third Map-Reduce in movies recommendation pipeline
 * 
 */
public class MoviesMR3 {

	public static class Map extends Mapper<LongWritable, Text, IntWritable, Text> {
		
		private int minUsers;
		@Override
		public void setup(Context context) {
			this.minUsers = context.getConfiguration().getInt(CONF_MIN_USERS, 0); 
		}
		public void map(LongWritable notUsed, Text value, Context context)	throws IOException, InterruptedException {
		  //TODO
		}
	}

	public static class Reduce extends Reducer<IntWritable, Text, IntWritable, Text> {
		
		private int topSize;
		@Override
		public void setup(Context context) {
			this.topSize = context.getConfiguration().getInt(CONF_TOP_SIZE, 5); 
		}
		public void reduce(IntWritable movie, Iterable<Text> movieSimilarities, Context context)	throws IOException, InterruptedException {
			//TODO
		}
		
		/**
	   * Compares two MovieSimilarity by (similarity DESC, numUsers DESC)
	   */
	  private static final Comparator<MovieSimilarity> SIMILARY_DESC_COMP = new Comparator<MovieSimilarity>() {
	    public int compare(MovieSimilarity o1, MovieSimilarity o2) {
	      int comp = -Doubles.compare(o1.similarity,o2.similarity);
	      return comp != 0 ? comp : -Ints.compare(o1.numUsers, o2.numUsers); 
	    }
	  };
	}
	
	
	/**
	 * Usage [MoviesMR2_output_dir] [output_dir] [top_size] [min_users] [num reducers]
	 */
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "movies_job3");
		job.setJarByClass(MoviesMR3.class);
		job.setMapOutputKeyClass(IntWritable.class);
		job.setMapOutputValueClass(Text.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);
		
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.getConfiguration().setInt(CONF_TOP_SIZE, Integer.parseInt(args[2]));
		job.getConfiguration().setInt(CONF_MIN_USERS, Integer.parseInt(args[3]));
		job.setNumReduceTasks(Integer.parseInt(args[4]));
		Utils.checkRunSuccesfully(job);
	}
	
	private static final String CONF_TOP_SIZE = MoviesMR3.class.getSimpleName() + ".top_size";
	private static final String CONF_MIN_USERS = MoviesMR3.class.getSimpleName() + ".min_users";
}
