package workshop.exercices.movies;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.google.common.collect.Lists;

import workshop.Utils;
import workshop.movies.utils.MovieRating;

/**
 *   Second Map-Reduce in movies recommendation pipeline
 */
public class MoviesMR2 {

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		public void map(LongWritable notUsed, Text value, Context context)	throws IOException, InterruptedException {
		  //TODO
		}
		
		public static List<MovieRating> toMovieRatings(String[] movieRatings) {
      List<MovieRating> result = Lists.newArrayList();
      for (int i = 0; i < movieRatings.length; i++) {
        result.add(MovieRating.parseMovieRating(movieRatings[i]));
      }
      return result;
    }
	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {
		public void reduce(Text moviePair, Iterable<Text> ratingPairs, Context context)	throws IOException, InterruptedException {
			//TODO
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "movies_job2");
		job.setJarByClass(MoviesMR2.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		Utils.checkRunSuccesfully(job);
	}
}
