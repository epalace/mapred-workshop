package workshop.exercices.deduplication;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import workshop.Utils;

import com.trovit.serialization.thrift.GeneralThriftSerialization;
import com.trovit.thrift.beans.Ad;

/**
 * This Hadoop job outputs ads ({@link Ad}) that have exactly the same title  
 */
public class TrovitAdDeduplication {

	public static class MyMapper extends Mapper<NullWritable, Ad, Text, Text> {

	  @Override
		public void map(NullWritable nothing, Ad ad, Context context)	throws IOException, InterruptedException {
	    //TODO
		}
	}

	public static class MyReducer extends Reducer<Text, Text, Text, Text> {

		@Override
		public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
      //TODO
    }
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		GeneralThriftSerialization.enableSerialization(conf);
		Job job = new Job(conf, "workshop-ad-deduplication");
		job.setJarByClass(TrovitAdDeduplication.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		
		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);

		job.setInputFormatClass(SequenceFileInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		Utils.checkRunSuccesfully(job);
	}
}
