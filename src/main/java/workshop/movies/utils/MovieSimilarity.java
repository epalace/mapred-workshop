package workshop.movies.utils;

/**
 * Defines a tuple of (movieId, similarity, numUsers)
 */
public class MovieSimilarity {
	public int movieId;
	public double similarity;
	public int numUsers;
	
	/** 
	 * This parses something like "13 0.63 102"  where movieId=13 , similarity=0.63 and numUsers=102  to {@link MovieSimilarity}
	 */
	public static MovieSimilarity parseMovieSimilarity(String s) {
		String[] tokens = s.split("\\s+");
		MovieSimilarity result = new MovieSimilarity();
		result.movieId = Integer.parseInt(tokens[0]);
		result.similarity = Double.parseDouble(tokens[1]);
		result.numUsers = Integer.parseInt(tokens[2]);
		return result;
	}
}