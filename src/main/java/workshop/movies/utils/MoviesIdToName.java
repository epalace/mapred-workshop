package workshop.movies.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;

import workshop.exercices.movies.MoviesMR3;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

/**
 * This util converts the output from {@link MoviesMR3} from movie ids to movie titles, using an input file containing the mapping (movieId=>movieTitle)
 */
public class MoviesIdToName {

	/**
	 * Usage : [movies_file] [MoviesMR3_output_dir] [output_file]
	 */
	public static void main(String[] args) throws Exception {
		Path itemsFile = new Path(args[0]);
		Path moviesMR3Output = new Path(args[1]);
		Path outputFile = new Path(args[2]);
		Configuration conf = new Configuration();
		System.out.println("Parsing item files");
		Map<Integer,String> idsToNames = parseItemFiles(conf,itemsFile);
		System.out.println(idsToNames.size() + " items parsed");
		FileSystem fs = moviesMR3Output.getFileSystem(conf);
		Path[] paths  = FileUtil.stat2Paths(fs.globStatus(new Path(moviesMR3Output, "part-r-*")));
		if ( paths.length == 0) {
		  System.err.println("Path '" +  moviesMR3Output + " doesn't contain part-r-* files inside");
		  System.exit(-1);
		}
		Writer outputWriter = new OutputStreamWriter(outputFile.getFileSystem(conf).create(outputFile),"UTF-8");
		for ( Path inputPath : paths) {
			System.out.println("Processing " + inputPath);
			processFile(fs,inputPath, outputWriter, idsToNames);
		}
		outputWriter.close();
		System.out.println("Done! Take a look at " +  outputFile);
	}
	
	private static void processFile(FileSystem fs,Path inputPath, Writer outputPath, Map<Integer,String> idsToNames) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(inputPath)));
		String line = null;
		while ((line = reader.readLine()) != null){
			//format is : movieId  \t    movieId1 similarity1 numUsers1; movieId2 similarity2 numUsers2; movieId3 ....
			String[] keyValue = line.split("\t");
			int movie1Id = Integer.parseInt(keyValue[0]);
			List<MovieSimilarity> similarities = parseSimilarities(keyValue[1].split(";"));
			String movie1Name = idsToNames.get(movie1Id);
			outputPath.append(movie1Id + " " + movie1Name).append("\n============\n");
			for ( MovieSimilarity i : similarities) {
				outputPath.append("\t" + i.movieId + " " + idsToNames.get(i.movieId) + " " + i.similarity + " " + i.numUsers).append("\n");
			}
			outputPath.append("\n");
		}
		reader.close();
	}
	
	private static List<MovieSimilarity> parseSimilarities(String[] similarities) {
		List<MovieSimilarity> result = Lists.newArrayList();
		for ( String item : similarities) {
			result.add(MovieSimilarity.parseMovieSimilarity(item));
		}
		return result;
	}
	
	private static Map<Integer,String> parseItemFiles(Configuration conf,Path path) throws IOException {
	  FileSystem fs = path.getFileSystem(conf);
		BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(path),"UTF-8"));
		String line = null;
		ImmutableMap.Builder<Integer,String> b = ImmutableMap.builder();
		while ( (line = reader.readLine()) != null) {
			line = line.trim();
			if (!line.isEmpty()){
  			String[] tokens = line.split("::");
  			int movieId = Integer.parseInt(tokens[0]);
  			String name = tokens[1];
  			b.put(movieId, name);
			}
		}
		reader.close();
		return b.build();
	}
	
}
