package workshop.movies.utils;

import java.util.Comparator;

import com.google.common.primitives.Ints;

/**
 * Pair of (movieId,rating)
 *
 */
public class MovieRating {
	public int movieId;
	public double rating;

	/**
	 * Creates a {@link MovieRating} from text with format : "movieId rating" 
	 */
	public static MovieRating parseMovieRating(String s) {
		String[] tokens = s.split("\\s+");
		MovieRating result = new MovieRating();
		result.movieId = Integer.parseInt(tokens[0]);
		result.rating = Double.parseDouble(tokens[1]);
		return result;
	}
	
	public static final Comparator<MovieRating> MOVIE_ID_COMP = new Comparator<MovieRating>() {
		public int compare(MovieRating one, MovieRating two) {
			return Ints.compare(one.movieId, two.movieId);
		}
	};

}