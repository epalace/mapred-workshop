package workshop.solutions.adcounter;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import workshop.Utils;

import com.trovit.serialization.thrift.GeneralThriftSerialization;
import com.trovit.serialization.thrift.ThriftSequenceFileInputFormat;
import com.trovit.thrift.beans.Ad;

/**
 * This Hadoop job calculates the document frequency of words.
 * 
 * The document frequency of a word is the number of unique documents it appears on. In this case a document is a Trovit ad ({@link Ad})
 *
 */
public class TrovitAdCounter {

	public static class MyMapper extends Mapper<NullWritable, Ad, Text, IntWritable> {
	  private final static IntWritable one = new IntWritable(1);
	  
	  @Override
		public void map(NullWritable nothing, Ad ad, Context context)	throws IOException, InterruptedException {
	    List<String> tokens = Arrays.asList(ad.getContent().split("\\s+"));
	    Set<String> dedupTokens = new HashSet<String>(tokens);
	    for(String token : dedupTokens) {
	      context.write(new Text(token), one);
	    }
		}
	}

	public static class MyReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
		@Override
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
      int count = 0;
      for (IntWritable val : values) {
        count+=val.get();
      }
      context.write(key, new IntWritable(count));
    }
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		GeneralThriftSerialization.enableSerialization(conf);
		Job job = new Job(conf, "workshop-ad-counter");
		job.setJarByClass(TrovitAdCounter.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.setMapperClass(MyMapper.class);
		job.setReducerClass(MyReducer.class);
		job.setCombinerClass(MyReducer.class);

		job.setInputFormatClass(ThriftSequenceFileInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		Utils.checkRunSuccesfully(job);
	}
}
