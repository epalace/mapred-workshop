package workshop.solutions.movies;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import workshop.Utils;

/**
 * This Hadoop job takes as input text files containing per line:<br> 
 * userId movieId rating <br>
 * 
 * groups them by "userId" and extracts as output : <br> 
 * 
 * userId => movieId1 rating1; movieId2 rating2 ; movieId3 rating3 ; .... movieIdN ratingN 
 *
 */
public class MoviesMR1 {
	
	public static class Map extends Mapper<LongWritable, Text, IntWritable, Text> {
	  
		public void map(LongWritable notUsed, Text value, Context context)	throws IOException, InterruptedException {
			String line = value.toString();
			String[] tokens = line.split("::");
			int userId = Integer.parseInt(tokens[0]);
			int movieId = Integer.parseInt(tokens[1]);
			double rating = Double.parseDouble(tokens[2]);
			context.write(new IntWritable(userId), new Text(movieId + " " + rating));
		}
	}

	public static class Reduce extends Reducer<IntWritable, Text, IntWritable, Text> {
		public void reduce(IntWritable userId, Iterable<Text> movieRatings, Context context)	throws IOException, InterruptedException {
			StringBuilder b = new StringBuilder();
			Iterator<Text> it = movieRatings.iterator();
			Text firstMovieRating = it.next();
			b.append(firstMovieRating);
			while ( it.hasNext()){
				b.append(";").append(it.next());
			}
			context.write(userId, new Text(b.toString()));
		}
	}

	/**
	 * Usage [input_files] [output_dir] [num_reducers]
	 */
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "movies_job1");
		job.setJarByClass(MoviesMR1.class);
		job.setOutputKeyClass(IntWritable.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		Utils.checkRunSuccesfully(job);
		
	}
}
