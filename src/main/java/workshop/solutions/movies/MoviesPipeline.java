package workshop.solutions.movies;

import java.io.File;

import org.apache.commons.io.FileUtils;

import workshop.solutions.movies.MoviesMR1;
import workshop.solutions.movies.MoviesMR2;
import workshop.solutions.movies.MoviesMR3;
import workshop.movies.utils.MoviesIdToName;

/**
 * Executes consecutively {@link MoviesMR1} ,{@link MoviesMR2} and {@link MoviesMR3}   
 */
public class MoviesPipeline {

  public static void main(String[] args) throws Exception {
    if ( args.length != 4) {
      System.err.println("Usage [ratingsFiles] [moviesFile] [outputDir] [num_reducers]");
      System.exit(-1);
    }
//    String ratingsFile = "src/test/resources/movies/ratings1m.txt";
//    String moviesFile = "src/test/resources/movies/movies.txt";
//    String pipelineDir = "pipeline_dir";
    String ratingsFile = args[0];
    String moviesFile = args[1];
    String pipelineDir = args[2];
    int numReducers = Integer.parseInt(args[3]);
    
    String mr1Dir = pipelineDir + "/mr1";
    String mr2Dir = pipelineDir + "/mr2";
    String mr3Dir = pipelineDir + "/mr3";
    String finalFile = pipelineDir + "/final.txt";
    
    //Let's remove pipeline folder if exists
    FileUtils.deleteDirectory(new File(pipelineDir));
    
    System.out.println("Exec Pipeline ( Step 1 / 3)");
    MoviesMR1.main(a(ratingsFile,mr1Dir,numReducers + ""));
    
    System.out.println("Exec Pipeline ( Step 2 / 3)");
    MoviesMR2.main(a(mr1Dir, mr2Dir,numReducers+""));
    
    System.out.println("Exec Pipeline ( Step 3 / 3)");
    MoviesMR3.main(a(mr2Dir, mr3Dir, "20", "100","1"));
    
    System.out.println("Exec Pipeline ( Converting ids to names)");
    MoviesIdToName.main(a(moviesFile,mr3Dir,finalFile));
  }
  
  private static String[] a(String ... strs) {
    return strs;
  }
  
}
