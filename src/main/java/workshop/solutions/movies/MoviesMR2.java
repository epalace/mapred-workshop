package workshop.solutions.movies;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import workshop.Utils;
import workshop.movies.utils.MovieRating;

import com.google.common.collect.Lists;

/**
 * This Hadoop job takes as input text files containing per line : 
 * userId1 \t movieId1 rating1; movieId2 rating2; movieId2 rating3; ...
 * userId2 \t movieId1 rating1; movieId2 rating2; ...
 * ...
 * 
 * and calculates the Pearson coefficient for every pair of movies (movie1,movie2) that at least there exists one user that 
 * rated both movies. 
 * 
 * The output hast this look(per line): 
 * movie1 movie2 pearsonCoefficient1_2 numUsers_1_2
 * movie1 movie3 pearsonCoefficient1_3 numUsers_1_3
 * movie1 ...
 * 
 * movie2 movie1 pearsonCoefficient2_1 numUsers_2_1
 * ...
 * movie3 movie1 ....
 * ...
 *
 */
public class MoviesMR2 {

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {
		public void map(LongWritable notUsed, Text value, Context context)	throws IOException, InterruptedException {
			String line = value.toString();
			String[] userAndMovies = line.split("\t");
			@SuppressWarnings("unused")	int userId = Integer.parseInt(userAndMovies[0]);
			String[] tokens = userAndMovies[1].split(";");
			List<MovieRating> movieRatings = toMovieRatings(tokens);
			//Only need to output one key for every DISTINCT movie1,movie2 pair. It halves intermediate output 
			//sort (movieId,rating) by movieId. Always output movie pairs (movieI,movieJ) where I <= J
			Collections.sort(movieRatings, MovieRating.MOVIE_ID_COMP);
			for ( int i=0 ; i < tokens.length ; i++){
				MovieRating movie1 = movieRatings.get(i);
				for ( int j = i+1; j < tokens.length ; j++){
					MovieRating movie2 = movieRatings.get(j);
					context.write(new Text(movie1.movieId + " " + movie2.movieId), new Text(movie1.rating + " " + movie2.rating));
				}
			}
		}
		
		public static List<MovieRating> toMovieRatings(String[] movieRatings) {
			List<MovieRating> result = Lists.newArrayList();
			for (int i = 0; i < movieRatings.length; i++) {
				result.add(MovieRating.parseMovieRating(movieRatings[i]));
			}
			return result;
		}
		
	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {
		public void reduce(Text moviePair, Iterable<Text> ratingPairs, Context context)	throws IOException, InterruptedException {
			int numUsers = 0;
			double sumx = 0;
			double sumy = 0;
			double sumx2 = 0;
			double sumy2 = 0;
			double sumxy = 0;
			
			for ( Text ratingPair : ratingPairs){
				String[] tokens = ratingPair.toString().split("\\s+");
				double x = Double.parseDouble(tokens[0]); //rating movie1
				double y = Double.parseDouble(tokens[1]); //rating movie2
				sumx += x;
				sumy += y;
				sumx2 += x * x;
				sumy2 += y * y;
				sumxy += x * y;
				numUsers++;
			}
			
			double numerator = numUsers * sumxy - sumx*sumy;
			double denom = Math.sqrt(numUsers * sumx2 - sumx*sumx) * Math.sqrt(numUsers * sumy2 - sumy*sumy);
			if ( denom != 0) {
				double pearson = numerator / denom;
				context.write(moviePair, new Text(pearson +  " " + numUsers));
			}
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();

		Job job = new Job(conf, "movies_job2");
		job.setJarByClass(MoviesMR2.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		job.setNumReduceTasks(Integer.parseInt(args[2]));
		Utils.checkRunSuccesfully(job);
	}
}
