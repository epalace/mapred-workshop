package workshop;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import com.google.common.base.Throwables;

public class Utils {

  /**
   * Checks that the specified job runs succesfully
   */
  public static void checkRunSuccesfully(Job job) {
    try{
      Path outputPath = FileOutputFormat.getOutputPath(job);
      FileSystem fs = outputPath.getFileSystem(job.getConfiguration());
      fs.delete(outputPath, true);
      job.waitForCompletion(true);
      if (!job.isSuccessful()) {
        throw new RuntimeException("The job '" + job.getJobName() + " didn't finish sucessfully");
      }
    } catch(Exception e){
      Throwables.propagate(e);
    }
  }
  
}
