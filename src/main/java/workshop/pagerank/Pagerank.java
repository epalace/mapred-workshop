package workshop.pagerank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.primitives.Doubles;

public class Pagerank {

	public static void main(String[] args) throws IOException {
		FileReader reader = new FileReader(new File("src/test/resources/pagerank/input.txt"));
		BufferedReader r = new BufferedReader(reader);
		String line = r.readLine();
		String[] tokens = line.split(" ");
		final int numVertices = Integer.parseInt(tokens[0]);
		final int[] outDegree = new int[numVertices];
		double[] previousPageranks = new double[numVertices];
		double[] newPageRanks = new double[numVertices]; 
		List<Integer>[] incidentVertices = new List[numVertices];
		for ( int i=0 ; i < numVertices ; i++) {
			incidentVertices[i] = new ArrayList<Integer>();
			previousPageranks[i] = 1.0 / (double)numVertices;
		}
		System.out.println("NUM VERTICES : " + numVertices);
		line = r.readLine();
		int numEdges = 0;
		while (line !=null){
			tokens = line.split("\\s+");
			int vertex1 = Integer.parseInt(tokens[0])-1;
			int vertex2 = Integer.parseInt(tokens[1])-1;
			outDegree[vertex1]++;
			incidentVertices[vertex2].add(vertex1);
			line = r.readLine();
			numEdges++;
		}
		r.close();
		System.out.println("File read numEdges=" + numEdges);
		
		final double DAMP_FACTOR = 0.90;
		
		int ITERATIONS = 1000;
		for ( int n=0 ; n < ITERATIONS ; n++){
			for ( int i=0 ; i < numVertices ; i++){
				List<Integer> inVertices = incidentVertices[i];
				double sum = 0.0;
				for ( int inVertex : inVertices) {
 					sum+= previousPageranks[inVertex] / outDegree[inVertex];
 				}
				if ( outDegree[i] == 0) { //if a node has no outgoing links then all its pagerank is added to itself
					sum += previousPageranks[i];
				}
				newPageRanks[i] = (1 - DAMP_FACTOR)*(1.0/(double)numVertices) + DAMP_FACTOR * sum;
			}
			//SWAP
			double[] tmp = previousPageranks;
			previousPageranks = newPageRanks;
			newPageRanks = tmp;
		}
		
		List<Vertex> vertices = new ArrayList<Vertex>(numVertices);
		
		for ( int i=0 ; i < numVertices; i++){
			vertices.add(new Vertex(i, previousPageranks[i]));
		}
		final Comparator<Vertex> COMP = new Comparator<Vertex>() {
			public int compare(Vertex one, Vertex two) {
				return -Doubles.compare(one.pr, two.pr);
			}
		};
		Collections.sort(vertices, COMP);
		double sum = 0.0;
		for ( Vertex v : vertices) {
			System.out.println("Node_" + v.id + " " + v.pr + " in=" + incidentVertices[v.id].size() +  " out=" + outDegree[v.id]);
			sum += v.pr;
		}
		System.out.println("Sum probs " + sum);
	}
	
	private static class Vertex {
		private final int id;
		private final double pr;
		public Vertex(int id, double pr) {
			this.id = id;
			this.pr = pr;
		}
	}
	
}
