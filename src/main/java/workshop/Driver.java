package workshop;

import org.apache.hadoop.util.ProgramDriver;


public class Driver extends ProgramDriver {
  public Driver() throws Throwable {
    super();
    addClass("movies-mr1",workshop.exercices.movies.MoviesMR1.class, "Movies Recommender 1st job");
    addClass("movies-mr2",workshop.exercices.movies.MoviesMR2.class, "Movies Recommender 1st job");
    addClass("movies-mr3",workshop.exercices.movies.MoviesMR3.class, "Movies Recommender 1st job");
    addClass("movies-pipeline",workshop.exercices.movies.MoviesPipeline.class, "Movies Pipeline");
    addClass("wordcount", workshop.exercices.wordcount.WordCount.class, "Wordcount");
    addClass("ad-dedup", workshop.exercices.deduplication.TrovitAdDeduplication.class, "TrovitAd Deduplication");
    addClass("ad-counter", workshop.exercices.adcounter.TrovitAdCounter.class, "TrovitAd Counter");
    
    addClass("solution-movies-mr1",workshop.solutions.movies.MoviesMR1.class, "Movies Recommender 1st job");
    addClass("solution-movies-mr2",workshop.solutions.movies.MoviesMR2.class, "Movies Recommender 1st job");
    addClass("solution-movies-mr3",workshop.solutions.movies.MoviesMR3.class, "Movies Recommender 1st job");
    addClass("solution-movies-pipeline",workshop.solutions.movies.MoviesPipeline.class, "Movies Pipeline");
    addClass("solution-wordcount", workshop.solutions.wordcount.WordCount.class, "Wordcount");
    addClass("solution-ad-counter", workshop.solutions.adcounter.TrovitAdCounter.class, "TrovitAd Counter");
    
  }

  public static void main(String[] args) {
    try {
      Driver driver = new Driver();
      driver.driver(args);
    } catch (Throwable e) {
      e.printStackTrace();
      System.exit(-1);
    }
  }
}
