package workshop;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * Writable with two integers
 *
 */
public class IntPairWritable implements Writable{

  private int first,second;
  
  public IntPairWritable() { }
  
  public IntPairWritable(int first,int second) {
    this.first = first;
    this.second = second;
  }
  
  public int getFirst() {  return first;  }
  public void setFirst(int first) {  this.first = first;  }
  public int getSecond() {  return second;  }
  public void setSecond(int second) { this.second = second;  }


  @Override
  public void write(DataOutput out) throws IOException {
    out.writeInt(first);
    out.writeInt(second);
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    this.first = in.readInt();
    this.second = in.readInt();
  }
}
