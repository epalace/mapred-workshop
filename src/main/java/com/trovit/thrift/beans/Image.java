/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.trovit.thrift.beans;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Image implements org.apache.thrift.TBase<Image, Image._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("Image");

  private static final org.apache.thrift.protocol.TField NUMBER_FIELD_DESC = new org.apache.thrift.protocol.TField("number", org.apache.thrift.protocol.TType.I16, (short)1);
  private static final org.apache.thrift.protocol.TField RGB_FIELD_DESC = new org.apache.thrift.protocol.TField("rgb", org.apache.thrift.protocol.TType.LIST, (short)2);
  private static final org.apache.thrift.protocol.TField IMAGE_STATUS_FIELD_DESC = new org.apache.thrift.protocol.TField("imageStatus", org.apache.thrift.protocol.TType.I32, (short)3);
  private static final org.apache.thrift.protocol.TField THUMBNAILS_FIELD_DESC = new org.apache.thrift.protocol.TField("thumbnails", org.apache.thrift.protocol.TType.LIST, (short)4);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ImageStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ImageTupleSchemeFactory());
  }

  public short number; // optional
  public List<Double> rgb; // optional
  public int imageStatus; // optional
  public List<ThumbnailInfo> thumbnails; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    NUMBER((short)1, "number"),
    RGB((short)2, "rgb"),
    IMAGE_STATUS((short)3, "imageStatus"),
    THUMBNAILS((short)4, "thumbnails");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // NUMBER
          return NUMBER;
        case 2: // RGB
          return RGB;
        case 3: // IMAGE_STATUS
          return IMAGE_STATUS;
        case 4: // THUMBNAILS
          return THUMBNAILS;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __NUMBER_ISSET_ID = 0;
  private static final int __IMAGESTATUS_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.NUMBER,_Fields.RGB,_Fields.IMAGE_STATUS,_Fields.THUMBNAILS};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.NUMBER, new org.apache.thrift.meta_data.FieldMetaData("number", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I16)));
    tmpMap.put(_Fields.RGB, new org.apache.thrift.meta_data.FieldMetaData("rgb", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE))));
    tmpMap.put(_Fields.IMAGE_STATUS, new org.apache.thrift.meta_data.FieldMetaData("imageStatus", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.THUMBNAILS, new org.apache.thrift.meta_data.FieldMetaData("thumbnails", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, ThumbnailInfo.class))));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(Image.class, metaDataMap);
  }

  public Image() {
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public Image(Image other) {
    __isset_bitfield = other.__isset_bitfield;
    this.number = other.number;
    if (other.isSetRgb()) {
      List<Double> __this__rgb = new ArrayList<Double>();
      for (Double other_element : other.rgb) {
        __this__rgb.add(other_element);
      }
      this.rgb = __this__rgb;
    }
    this.imageStatus = other.imageStatus;
    if (other.isSetThumbnails()) {
      List<ThumbnailInfo> __this__thumbnails = new ArrayList<ThumbnailInfo>();
      for (ThumbnailInfo other_element : other.thumbnails) {
        __this__thumbnails.add(new ThumbnailInfo(other_element));
      }
      this.thumbnails = __this__thumbnails;
    }
  }

  public Image deepCopy() {
    return new Image(this);
  }

  @Override
  public void clear() {
    setNumberIsSet(false);
    this.number = 0;
    this.rgb = null;
    setImageStatusIsSet(false);
    this.imageStatus = 0;
    this.thumbnails = null;
  }

  public short getNumber() {
    return this.number;
  }

  public Image setNumber(short number) {
    this.number = number;
    setNumberIsSet(true);
    return this;
  }

  public void unsetNumber() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __NUMBER_ISSET_ID);
  }

  /** Returns true if field number is set (has been assigned a value) and false otherwise */
  public boolean isSetNumber() {
    return EncodingUtils.testBit(__isset_bitfield, __NUMBER_ISSET_ID);
  }

  public void setNumberIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __NUMBER_ISSET_ID, value);
  }

  public int getRgbSize() {
    return (this.rgb == null) ? 0 : this.rgb.size();
  }

  public java.util.Iterator<Double> getRgbIterator() {
    return (this.rgb == null) ? null : this.rgb.iterator();
  }

  public void addToRgb(double elem) {
    if (this.rgb == null) {
      this.rgb = new ArrayList<Double>();
    }
    this.rgb.add(elem);
  }

  public List<Double> getRgb() {
    return this.rgb;
  }

  public Image setRgb(List<Double> rgb) {
    this.rgb = rgb;
    return this;
  }

  public void unsetRgb() {
    this.rgb = null;
  }

  /** Returns true if field rgb is set (has been assigned a value) and false otherwise */
  public boolean isSetRgb() {
    return this.rgb != null;
  }

  public void setRgbIsSet(boolean value) {
    if (!value) {
      this.rgb = null;
    }
  }

  public int getImageStatus() {
    return this.imageStatus;
  }

  public Image setImageStatus(int imageStatus) {
    this.imageStatus = imageStatus;
    setImageStatusIsSet(true);
    return this;
  }

  public void unsetImageStatus() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __IMAGESTATUS_ISSET_ID);
  }

  /** Returns true if field imageStatus is set (has been assigned a value) and false otherwise */
  public boolean isSetImageStatus() {
    return EncodingUtils.testBit(__isset_bitfield, __IMAGESTATUS_ISSET_ID);
  }

  public void setImageStatusIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __IMAGESTATUS_ISSET_ID, value);
  }

  public int getThumbnailsSize() {
    return (this.thumbnails == null) ? 0 : this.thumbnails.size();
  }

  public java.util.Iterator<ThumbnailInfo> getThumbnailsIterator() {
    return (this.thumbnails == null) ? null : this.thumbnails.iterator();
  }

  public void addToThumbnails(ThumbnailInfo elem) {
    if (this.thumbnails == null) {
      this.thumbnails = new ArrayList<ThumbnailInfo>();
    }
    this.thumbnails.add(elem);
  }

  public List<ThumbnailInfo> getThumbnails() {
    return this.thumbnails;
  }

  public Image setThumbnails(List<ThumbnailInfo> thumbnails) {
    this.thumbnails = thumbnails;
    return this;
  }

  public void unsetThumbnails() {
    this.thumbnails = null;
  }

  /** Returns true if field thumbnails is set (has been assigned a value) and false otherwise */
  public boolean isSetThumbnails() {
    return this.thumbnails != null;
  }

  public void setThumbnailsIsSet(boolean value) {
    if (!value) {
      this.thumbnails = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case NUMBER:
      if (value == null) {
        unsetNumber();
      } else {
        setNumber((Short)value);
      }
      break;

    case RGB:
      if (value == null) {
        unsetRgb();
      } else {
        setRgb((List<Double>)value);
      }
      break;

    case IMAGE_STATUS:
      if (value == null) {
        unsetImageStatus();
      } else {
        setImageStatus((Integer)value);
      }
      break;

    case THUMBNAILS:
      if (value == null) {
        unsetThumbnails();
      } else {
        setThumbnails((List<ThumbnailInfo>)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case NUMBER:
      return Short.valueOf(getNumber());

    case RGB:
      return getRgb();

    case IMAGE_STATUS:
      return Integer.valueOf(getImageStatus());

    case THUMBNAILS:
      return getThumbnails();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case NUMBER:
      return isSetNumber();
    case RGB:
      return isSetRgb();
    case IMAGE_STATUS:
      return isSetImageStatus();
    case THUMBNAILS:
      return isSetThumbnails();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof Image)
      return this.equals((Image)that);
    return false;
  }

  public boolean equals(Image that) {
    if (that == null)
      return false;

    boolean this_present_number = true && this.isSetNumber();
    boolean that_present_number = true && that.isSetNumber();
    if (this_present_number || that_present_number) {
      if (!(this_present_number && that_present_number))
        return false;
      if (this.number != that.number)
        return false;
    }

    boolean this_present_rgb = true && this.isSetRgb();
    boolean that_present_rgb = true && that.isSetRgb();
    if (this_present_rgb || that_present_rgb) {
      if (!(this_present_rgb && that_present_rgb))
        return false;
      if (!this.rgb.equals(that.rgb))
        return false;
    }

    boolean this_present_imageStatus = true && this.isSetImageStatus();
    boolean that_present_imageStatus = true && that.isSetImageStatus();
    if (this_present_imageStatus || that_present_imageStatus) {
      if (!(this_present_imageStatus && that_present_imageStatus))
        return false;
      if (this.imageStatus != that.imageStatus)
        return false;
    }

    boolean this_present_thumbnails = true && this.isSetThumbnails();
    boolean that_present_thumbnails = true && that.isSetThumbnails();
    if (this_present_thumbnails || that_present_thumbnails) {
      if (!(this_present_thumbnails && that_present_thumbnails))
        return false;
      if (!this.thumbnails.equals(that.thumbnails))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(Image other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    Image typedOther = (Image)other;

    lastComparison = Boolean.valueOf(isSetNumber()).compareTo(typedOther.isSetNumber());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetNumber()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.number, typedOther.number);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetRgb()).compareTo(typedOther.isSetRgb());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetRgb()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.rgb, typedOther.rgb);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetImageStatus()).compareTo(typedOther.isSetImageStatus());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetImageStatus()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.imageStatus, typedOther.imageStatus);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetThumbnails()).compareTo(typedOther.isSetThumbnails());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetThumbnails()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.thumbnails, typedOther.thumbnails);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Image(");
    boolean first = true;

    if (isSetNumber()) {
      sb.append("number:");
      sb.append(this.number);
      first = false;
    }
    if (isSetRgb()) {
      if (!first) sb.append(", ");
      sb.append("rgb:");
      if (this.rgb == null) {
        sb.append("null");
      } else {
        sb.append(this.rgb);
      }
      first = false;
    }
    if (isSetImageStatus()) {
      if (!first) sb.append(", ");
      sb.append("imageStatus:");
      sb.append(this.imageStatus);
      first = false;
    }
    if (isSetThumbnails()) {
      if (!first) sb.append(", ");
      sb.append("thumbnails:");
      if (this.thumbnails == null) {
        sb.append("null");
      } else {
        sb.append(this.thumbnails);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ImageStandardSchemeFactory implements SchemeFactory {
    public ImageStandardScheme getScheme() {
      return new ImageStandardScheme();
    }
  }

  private static class ImageStandardScheme extends StandardScheme<Image> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, Image struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // NUMBER
            if (schemeField.type == org.apache.thrift.protocol.TType.I16) {
              struct.number = iprot.readI16();
              struct.setNumberIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // RGB
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list182 = iprot.readListBegin();
                struct.rgb = new ArrayList<Double>(_list182.size);
                for (int _i183 = 0; _i183 < _list182.size; ++_i183)
                {
                  double _elem184; // required
                  _elem184 = iprot.readDouble();
                  struct.rgb.add(_elem184);
                }
                iprot.readListEnd();
              }
              struct.setRgbIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // IMAGE_STATUS
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.imageStatus = iprot.readI32();
              struct.setImageStatusIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // THUMBNAILS
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list185 = iprot.readListBegin();
                struct.thumbnails = new ArrayList<ThumbnailInfo>(_list185.size);
                for (int _i186 = 0; _i186 < _list185.size; ++_i186)
                {
                  ThumbnailInfo _elem187; // required
                  _elem187 = new ThumbnailInfo();
                  _elem187.read(iprot);
                  struct.thumbnails.add(_elem187);
                }
                iprot.readListEnd();
              }
              struct.setThumbnailsIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, Image struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.isSetNumber()) {
        oprot.writeFieldBegin(NUMBER_FIELD_DESC);
        oprot.writeI16(struct.number);
        oprot.writeFieldEnd();
      }
      if (struct.rgb != null) {
        if (struct.isSetRgb()) {
          oprot.writeFieldBegin(RGB_FIELD_DESC);
          {
            oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.DOUBLE, struct.rgb.size()));
            for (double _iter188 : struct.rgb)
            {
              oprot.writeDouble(_iter188);
            }
            oprot.writeListEnd();
          }
          oprot.writeFieldEnd();
        }
      }
      if (struct.isSetImageStatus()) {
        oprot.writeFieldBegin(IMAGE_STATUS_FIELD_DESC);
        oprot.writeI32(struct.imageStatus);
        oprot.writeFieldEnd();
      }
      if (struct.thumbnails != null) {
        if (struct.isSetThumbnails()) {
          oprot.writeFieldBegin(THUMBNAILS_FIELD_DESC);
          {
            oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, struct.thumbnails.size()));
            for (ThumbnailInfo _iter189 : struct.thumbnails)
            {
              _iter189.write(oprot);
            }
            oprot.writeListEnd();
          }
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ImageTupleSchemeFactory implements SchemeFactory {
    public ImageTupleScheme getScheme() {
      return new ImageTupleScheme();
    }
  }

  private static class ImageTupleScheme extends TupleScheme<Image> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, Image struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetNumber()) {
        optionals.set(0);
      }
      if (struct.isSetRgb()) {
        optionals.set(1);
      }
      if (struct.isSetImageStatus()) {
        optionals.set(2);
      }
      if (struct.isSetThumbnails()) {
        optionals.set(3);
      }
      oprot.writeBitSet(optionals, 4);
      if (struct.isSetNumber()) {
        oprot.writeI16(struct.number);
      }
      if (struct.isSetRgb()) {
        {
          oprot.writeI32(struct.rgb.size());
          for (double _iter190 : struct.rgb)
          {
            oprot.writeDouble(_iter190);
          }
        }
      }
      if (struct.isSetImageStatus()) {
        oprot.writeI32(struct.imageStatus);
      }
      if (struct.isSetThumbnails()) {
        {
          oprot.writeI32(struct.thumbnails.size());
          for (ThumbnailInfo _iter191 : struct.thumbnails)
          {
            _iter191.write(oprot);
          }
        }
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, Image struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(4);
      if (incoming.get(0)) {
        struct.number = iprot.readI16();
        struct.setNumberIsSet(true);
      }
      if (incoming.get(1)) {
        {
          org.apache.thrift.protocol.TList _list192 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.DOUBLE, iprot.readI32());
          struct.rgb = new ArrayList<Double>(_list192.size);
          for (int _i193 = 0; _i193 < _list192.size; ++_i193)
          {
            double _elem194; // required
            _elem194 = iprot.readDouble();
            struct.rgb.add(_elem194);
          }
        }
        struct.setRgbIsSet(true);
      }
      if (incoming.get(2)) {
        struct.imageStatus = iprot.readI32();
        struct.setImageStatusIsSet(true);
      }
      if (incoming.get(3)) {
        {
          org.apache.thrift.protocol.TList _list195 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, iprot.readI32());
          struct.thumbnails = new ArrayList<ThumbnailInfo>(_list195.size);
          for (int _i196 = 0; _i196 < _list195.size; ++_i196)
          {
            ThumbnailInfo _elem197; // required
            _elem197 = new ThumbnailInfo();
            _elem197.read(iprot);
            struct.thumbnails.add(_elem197);
          }
        }
        struct.setThumbnailsIsSet(true);
      }
    }
  }

}

