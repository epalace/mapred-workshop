/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.trovit.thrift.beans;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Kw2Ad implements org.apache.thrift.TBase<Kw2Ad, Kw2Ad._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("Kw2Ad");

  private static final org.apache.thrift.protocol.TField QUERY_FIELD_DESC = new org.apache.thrift.protocol.TField("query", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField WHAT_FIELD_DESC = new org.apache.thrift.protocol.TField("what", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField WHERE_FIELD_DESC = new org.apache.thrift.protocol.TField("where", org.apache.thrift.protocol.TType.STRING, (short)3);
  private static final org.apache.thrift.protocol.TField COUNT_FIELD_DESC = new org.apache.thrift.protocol.TField("count", org.apache.thrift.protocol.TType.I32, (short)4);
  private static final org.apache.thrift.protocol.TField NEW_WHERE_FIELD_DESC = new org.apache.thrift.protocol.TField("newWhere", org.apache.thrift.protocol.TType.BOOL, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new Kw2AdStandardSchemeFactory());
    schemes.put(TupleScheme.class, new Kw2AdTupleSchemeFactory());
  }

  public String query; // optional
  public String what; // optional
  public String where; // optional
  public int count; // optional
  public boolean newWhere; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    QUERY((short)1, "query"),
    WHAT((short)2, "what"),
    WHERE((short)3, "where"),
    COUNT((short)4, "count"),
    NEW_WHERE((short)5, "newWhere");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // QUERY
          return QUERY;
        case 2: // WHAT
          return WHAT;
        case 3: // WHERE
          return WHERE;
        case 4: // COUNT
          return COUNT;
        case 5: // NEW_WHERE
          return NEW_WHERE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __COUNT_ISSET_ID = 0;
  private static final int __NEWWHERE_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.QUERY,_Fields.WHAT,_Fields.WHERE,_Fields.COUNT,_Fields.NEW_WHERE};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.QUERY, new org.apache.thrift.meta_data.FieldMetaData("query", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.WHAT, new org.apache.thrift.meta_data.FieldMetaData("what", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.WHERE, new org.apache.thrift.meta_data.FieldMetaData("where", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.COUNT, new org.apache.thrift.meta_data.FieldMetaData("count", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.NEW_WHERE, new org.apache.thrift.meta_data.FieldMetaData("newWhere", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.BOOL)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(Kw2Ad.class, metaDataMap);
  }

  public Kw2Ad() {
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public Kw2Ad(Kw2Ad other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetQuery()) {
      this.query = other.query;
    }
    if (other.isSetWhat()) {
      this.what = other.what;
    }
    if (other.isSetWhere()) {
      this.where = other.where;
    }
    this.count = other.count;
    this.newWhere = other.newWhere;
  }

  public Kw2Ad deepCopy() {
    return new Kw2Ad(this);
  }

  @Override
  public void clear() {
    this.query = null;
    this.what = null;
    this.where = null;
    setCountIsSet(false);
    this.count = 0;
    setNewWhereIsSet(false);
    this.newWhere = false;
  }

  public String getQuery() {
    return this.query;
  }

  public Kw2Ad setQuery(String query) {
    this.query = query;
    return this;
  }

  public void unsetQuery() {
    this.query = null;
  }

  /** Returns true if field query is set (has been assigned a value) and false otherwise */
  public boolean isSetQuery() {
    return this.query != null;
  }

  public void setQueryIsSet(boolean value) {
    if (!value) {
      this.query = null;
    }
  }

  public String getWhat() {
    return this.what;
  }

  public Kw2Ad setWhat(String what) {
    this.what = what;
    return this;
  }

  public void unsetWhat() {
    this.what = null;
  }

  /** Returns true if field what is set (has been assigned a value) and false otherwise */
  public boolean isSetWhat() {
    return this.what != null;
  }

  public void setWhatIsSet(boolean value) {
    if (!value) {
      this.what = null;
    }
  }

  public String getWhere() {
    return this.where;
  }

  public Kw2Ad setWhere(String where) {
    this.where = where;
    return this;
  }

  public void unsetWhere() {
    this.where = null;
  }

  /** Returns true if field where is set (has been assigned a value) and false otherwise */
  public boolean isSetWhere() {
    return this.where != null;
  }

  public void setWhereIsSet(boolean value) {
    if (!value) {
      this.where = null;
    }
  }

  public int getCount() {
    return this.count;
  }

  public Kw2Ad setCount(int count) {
    this.count = count;
    setCountIsSet(true);
    return this;
  }

  public void unsetCount() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __COUNT_ISSET_ID);
  }

  /** Returns true if field count is set (has been assigned a value) and false otherwise */
  public boolean isSetCount() {
    return EncodingUtils.testBit(__isset_bitfield, __COUNT_ISSET_ID);
  }

  public void setCountIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __COUNT_ISSET_ID, value);
  }

  public boolean isNewWhere() {
    return this.newWhere;
  }

  public Kw2Ad setNewWhere(boolean newWhere) {
    this.newWhere = newWhere;
    setNewWhereIsSet(true);
    return this;
  }

  public void unsetNewWhere() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __NEWWHERE_ISSET_ID);
  }

  /** Returns true if field newWhere is set (has been assigned a value) and false otherwise */
  public boolean isSetNewWhere() {
    return EncodingUtils.testBit(__isset_bitfield, __NEWWHERE_ISSET_ID);
  }

  public void setNewWhereIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __NEWWHERE_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case QUERY:
      if (value == null) {
        unsetQuery();
      } else {
        setQuery((String)value);
      }
      break;

    case WHAT:
      if (value == null) {
        unsetWhat();
      } else {
        setWhat((String)value);
      }
      break;

    case WHERE:
      if (value == null) {
        unsetWhere();
      } else {
        setWhere((String)value);
      }
      break;

    case COUNT:
      if (value == null) {
        unsetCount();
      } else {
        setCount((Integer)value);
      }
      break;

    case NEW_WHERE:
      if (value == null) {
        unsetNewWhere();
      } else {
        setNewWhere((Boolean)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case QUERY:
      return getQuery();

    case WHAT:
      return getWhat();

    case WHERE:
      return getWhere();

    case COUNT:
      return Integer.valueOf(getCount());

    case NEW_WHERE:
      return Boolean.valueOf(isNewWhere());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case QUERY:
      return isSetQuery();
    case WHAT:
      return isSetWhat();
    case WHERE:
      return isSetWhere();
    case COUNT:
      return isSetCount();
    case NEW_WHERE:
      return isSetNewWhere();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof Kw2Ad)
      return this.equals((Kw2Ad)that);
    return false;
  }

  public boolean equals(Kw2Ad that) {
    if (that == null)
      return false;

    boolean this_present_query = true && this.isSetQuery();
    boolean that_present_query = true && that.isSetQuery();
    if (this_present_query || that_present_query) {
      if (!(this_present_query && that_present_query))
        return false;
      if (!this.query.equals(that.query))
        return false;
    }

    boolean this_present_what = true && this.isSetWhat();
    boolean that_present_what = true && that.isSetWhat();
    if (this_present_what || that_present_what) {
      if (!(this_present_what && that_present_what))
        return false;
      if (!this.what.equals(that.what))
        return false;
    }

    boolean this_present_where = true && this.isSetWhere();
    boolean that_present_where = true && that.isSetWhere();
    if (this_present_where || that_present_where) {
      if (!(this_present_where && that_present_where))
        return false;
      if (!this.where.equals(that.where))
        return false;
    }

    boolean this_present_count = true && this.isSetCount();
    boolean that_present_count = true && that.isSetCount();
    if (this_present_count || that_present_count) {
      if (!(this_present_count && that_present_count))
        return false;
      if (this.count != that.count)
        return false;
    }

    boolean this_present_newWhere = true && this.isSetNewWhere();
    boolean that_present_newWhere = true && that.isSetNewWhere();
    if (this_present_newWhere || that_present_newWhere) {
      if (!(this_present_newWhere && that_present_newWhere))
        return false;
      if (this.newWhere != that.newWhere)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(Kw2Ad other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    Kw2Ad typedOther = (Kw2Ad)other;

    lastComparison = Boolean.valueOf(isSetQuery()).compareTo(typedOther.isSetQuery());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetQuery()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.query, typedOther.query);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetWhat()).compareTo(typedOther.isSetWhat());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetWhat()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.what, typedOther.what);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetWhere()).compareTo(typedOther.isSetWhere());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetWhere()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.where, typedOther.where);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetCount()).compareTo(typedOther.isSetCount());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetCount()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.count, typedOther.count);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetNewWhere()).compareTo(typedOther.isSetNewWhere());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetNewWhere()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.newWhere, typedOther.newWhere);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Kw2Ad(");
    boolean first = true;

    if (isSetQuery()) {
      sb.append("query:");
      if (this.query == null) {
        sb.append("null");
      } else {
        sb.append(this.query);
      }
      first = false;
    }
    if (isSetWhat()) {
      if (!first) sb.append(", ");
      sb.append("what:");
      if (this.what == null) {
        sb.append("null");
      } else {
        sb.append(this.what);
      }
      first = false;
    }
    if (isSetWhere()) {
      if (!first) sb.append(", ");
      sb.append("where:");
      if (this.where == null) {
        sb.append("null");
      } else {
        sb.append(this.where);
      }
      first = false;
    }
    if (isSetCount()) {
      if (!first) sb.append(", ");
      sb.append("count:");
      sb.append(this.count);
      first = false;
    }
    if (isSetNewWhere()) {
      if (!first) sb.append(", ");
      sb.append("newWhere:");
      sb.append(this.newWhere);
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class Kw2AdStandardSchemeFactory implements SchemeFactory {
    public Kw2AdStandardScheme getScheme() {
      return new Kw2AdStandardScheme();
    }
  }

  private static class Kw2AdStandardScheme extends StandardScheme<Kw2Ad> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, Kw2Ad struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // QUERY
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.query = iprot.readString();
              struct.setQueryIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // WHAT
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.what = iprot.readString();
              struct.setWhatIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // WHERE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.where = iprot.readString();
              struct.setWhereIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // COUNT
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.count = iprot.readI32();
              struct.setCountIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // NEW_WHERE
            if (schemeField.type == org.apache.thrift.protocol.TType.BOOL) {
              struct.newWhere = iprot.readBool();
              struct.setNewWhereIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, Kw2Ad struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.query != null) {
        if (struct.isSetQuery()) {
          oprot.writeFieldBegin(QUERY_FIELD_DESC);
          oprot.writeString(struct.query);
          oprot.writeFieldEnd();
        }
      }
      if (struct.what != null) {
        if (struct.isSetWhat()) {
          oprot.writeFieldBegin(WHAT_FIELD_DESC);
          oprot.writeString(struct.what);
          oprot.writeFieldEnd();
        }
      }
      if (struct.where != null) {
        if (struct.isSetWhere()) {
          oprot.writeFieldBegin(WHERE_FIELD_DESC);
          oprot.writeString(struct.where);
          oprot.writeFieldEnd();
        }
      }
      if (struct.isSetCount()) {
        oprot.writeFieldBegin(COUNT_FIELD_DESC);
        oprot.writeI32(struct.count);
        oprot.writeFieldEnd();
      }
      if (struct.isSetNewWhere()) {
        oprot.writeFieldBegin(NEW_WHERE_FIELD_DESC);
        oprot.writeBool(struct.newWhere);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class Kw2AdTupleSchemeFactory implements SchemeFactory {
    public Kw2AdTupleScheme getScheme() {
      return new Kw2AdTupleScheme();
    }
  }

  private static class Kw2AdTupleScheme extends TupleScheme<Kw2Ad> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, Kw2Ad struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetQuery()) {
        optionals.set(0);
      }
      if (struct.isSetWhat()) {
        optionals.set(1);
      }
      if (struct.isSetWhere()) {
        optionals.set(2);
      }
      if (struct.isSetCount()) {
        optionals.set(3);
      }
      if (struct.isSetNewWhere()) {
        optionals.set(4);
      }
      oprot.writeBitSet(optionals, 5);
      if (struct.isSetQuery()) {
        oprot.writeString(struct.query);
      }
      if (struct.isSetWhat()) {
        oprot.writeString(struct.what);
      }
      if (struct.isSetWhere()) {
        oprot.writeString(struct.where);
      }
      if (struct.isSetCount()) {
        oprot.writeI32(struct.count);
      }
      if (struct.isSetNewWhere()) {
        oprot.writeBool(struct.newWhere);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, Kw2Ad struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(5);
      if (incoming.get(0)) {
        struct.query = iprot.readString();
        struct.setQueryIsSet(true);
      }
      if (incoming.get(1)) {
        struct.what = iprot.readString();
        struct.setWhatIsSet(true);
      }
      if (incoming.get(2)) {
        struct.where = iprot.readString();
        struct.setWhereIsSet(true);
      }
      if (incoming.get(3)) {
        struct.count = iprot.readI32();
        struct.setCountIsSet(true);
      }
      if (incoming.get(4)) {
        struct.newWhere = iprot.readBool();
        struct.setNewWhereIsSet(true);
      }
    }
  }

}

