package com.trovit.serialization.thrift;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.SequenceFile.Metadata;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.ReflectionUtils;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;

/** An {@link OutputFormat} that writes {@link ThriftSequenceFile}s. */
public class ThriftSequenceFileOutputFormat <K,V> extends FileOutputFormat<K, V> {
  
  /**
   * {@link TTupleProtocol} is not supported since doesn't allow schema-evolution.
   * {@link TTupleProtocol} is only supported in map-output 
   */
  public static final ImmutableSet<Class<? extends TProtocol>> SUPPORTED_OUTPUT_PROTOCOLS = 
      ImmutableSet.of(TCompactProtocol.class, TBinaryProtocol.class); 
  
  public RecordWriter<K, V> 
         getRecordWriter(TaskAttemptContext context
                         ) throws IOException, InterruptedException {
    Configuration conf = context.getConfiguration();
    CompressionCodec codec = null;
    CompressionType compressionType = CompressionType.NONE;
    if (SequenceFileOutputFormat.getCompressOutput(context)) {
      // find the kind of compression to do
      compressionType = SequenceFileOutputFormat.getOutputCompressionType(context);
      Class<?> codecClass = getOutputCompressorClass(context, DefaultCodec.class);
      codec = (CompressionCodec)   ReflectionUtils.newInstance(codecClass, conf);
    }
    // get the path of the temporary output file 
    Path file = getDefaultWorkFile(context, "");
    FileSystem fs = file.getFileSystem(conf);
    Metadata metadata = new Metadata();
    
    Class<? extends TProtocol> protocolClazz = ThriftSequenceFile.getOutputThriftProtocolSerialization(conf);
    
    if ( protocolClazz == null){
      protocolClazz = TCompactProtocol.class;
    } else {
      Preconditions.checkState(SUPPORTED_OUTPUT_PROTOCOLS.contains(protocolClazz),
          "Only supported output protocols " + SUPPORTED_OUTPUT_PROTOCOLS);
    }
    
    ThriftSequenceFile.setMetadataThriftProtocol(metadata, protocolClazz);
    Configuration newConf = new Configuration(conf);
    GeneralThriftSerialization.setThriftProtocolSerialization(newConf, protocolClazz);
    
    final SequenceFile.Writer out = 
      SequenceFile.createWriter(fs, newConf, file,
                                context.getOutputKeyClass(),
                                context.getOutputValueClass(),
                                compressionType,
                                codec,
                                context,metadata);

    return new RecordWriter<K, V>() {
        public void write(K key, V value) throws IOException {
          out.append(key, value);
        }

        public void close(TaskAttemptContext context) throws IOException { 
          out.close();
        }
      };
  }

}

