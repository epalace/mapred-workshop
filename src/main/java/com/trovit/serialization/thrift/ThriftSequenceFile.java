package com.trovit.serialization.thrift;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.SequenceFile.Metadata;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.hadoop.io.compress.DefaultCodec;
import org.apache.hadoop.util.Progressable;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;

import com.google.common.base.Preconditions;

/**
 * Class that deals with sequence files that contain metadata that specify which {@link TProtocol} is used to serialize Thrift objects
 * 
 */
public class ThriftSequenceFile {
  
  /**
   * !!!NEVER NEVER TOUCH THIS STRING!!. This is used in sequence file metadata to specify which Thrift protocol is used.
   * 
   */
  private static final String METADATA_THRIFT_SERIALIZATION_PROTOCOL= "sf.metadata.thrift.serialization.protocol.class";
  
  /**
   * 
   */
  private static final String CONF_THRIFT_OUTPUT_FORMAT_PROTOCOL = ThriftSequenceFile.class.getName() + "-thrift.serialization.protocol";
  
  
  public static Class<? extends TProtocol> getOutputThriftProtocolSerialization(Configuration conf){
    return conf.getClass(CONF_THRIFT_OUTPUT_FORMAT_PROTOCOL, null, TProtocol.class);
  }
  
  public static void setOutputThriftProtocolSerialization(Configuration conf, Class<? extends TProtocol> protocolClass){
    conf.setClass(CONF_THRIFT_OUTPUT_FORMAT_PROTOCOL, protocolClass, TProtocol.class);
  }
  
  public static void setMetadataThriftProtocol(Metadata metadata, Class<? extends TProtocol> protocol){
    metadata.set(new Text(METADATA_THRIFT_SERIALIZATION_PROTOCOL), new Text(protocol.getName().toString()));
  }
  
  public static Class<? extends TProtocol> getMetadataThriftProtocol(Metadata metadata){
    Text classText = metadata.get(new Text(METADATA_THRIFT_SERIALIZATION_PROTOCOL));
    try {
      return classText == null ? null : (Class<? extends TProtocol>) Class.forName(classText.toString());
    } catch(ClassNotFoundException e){
      throw new IllegalStateException("Class " + classText + " not found",e);
    }
  }
  
  
  public static Class<? extends TProtocol> getThriftProtocol(FileSystem fs, Path file, Configuration conf) throws IOException {
    SequenceFile.Reader reader = null; 
    try {
      reader = new SequenceFile.Reader(fs, file, conf);
      return getMetadataThriftProtocol(reader.getMetadata());
    } finally {
      if ( reader != null){
        reader.close();
      }
    }
  }

  
  public static SequenceFile.Reader getReader(FileSystem fs, Path file, Configuration conf) throws IOException {
    Class<? extends TProtocol> protocol = getThriftProtocol(fs, file, conf);
    Configuration newConf = new Configuration(conf);
    GeneralThriftSerialization.enableSerialization(newConf);
    if ( protocol != null){
      GeneralThriftSerialization.setThriftProtocolSerialization(newConf, protocol);
    }
    return new SequenceFile.Reader(fs, file, newConf);
  }
  
  /** Create the named file with write-progress reporter. */
  public static SequenceFile.Writer createWriter(FileSystem fs, Configuration conf, Path name, 
                Class keyClass, Class valClass) throws IOException {
    return createWriter(fs, conf, name, keyClass, valClass,TCompactProtocol.class);
  }
  
  
  /** Create the named file with write-progress reporter. */
  public static SequenceFile.Writer createWriter(FileSystem fs, Configuration conf, Path name, 
                Class keyClass, Class valClass,Class<? extends TProtocol> protocolClass) throws IOException {
    CompressionType compressionType = SequenceFile.getDefaultCompressionType(conf);
    return createWriter(fs, conf, name, keyClass, valClass,protocolClass,compressionType,new DefaultCodec() );
  }
  
  public static SequenceFile.Writer createWriter(FileSystem fs, Configuration conf, Path name, 
      Class keyClass, Class valClass,Class<? extends TProtocol> protocolClass, CompressionType compressionType, CompressionCodec codec) throws IOException {
    return createWriter(fs, conf, name, keyClass, valClass, protocolClass, compressionType, codec, fs.getConf().getInt("io.file.buffer.size", 4096),
         fs.getDefaultReplication(), fs.getDefaultBlockSize(),
         null, new Metadata());
  }
  
  /** Create the named file with write-progress reporter. */
  public static SequenceFile.Writer createWriter (FileSystem fs, Configuration conf, Path name, 
                Class keyClass, Class valClass,Class<? extends TProtocol> protocol, 
                CompressionType compressionType,CompressionCodec codec,int bufferSize, 
                short replication, long blockSize,Progressable progress, Metadata metadata) throws IOException {
    Preconditions.checkNotNull(protocol, "Thrift protocol serialization can't be null");
    Class<? extends TProtocol> protocolAlreadyInMetadata =getMetadataThriftProtocol(metadata);
    if ( protocolAlreadyInMetadata != null){
      throw new IllegalStateException("Thrift protocol serialization already in metadata : " + protocolAlreadyInMetadata);
    }
   Metadata newMetadata = new Metadata(metadata.getMetadata());
   setMetadataThriftProtocol(newMetadata, protocol);
   Configuration newConf = new Configuration(conf);
   GeneralThriftSerialization.enableSerialization(newConf);
   GeneralThriftSerialization.setThriftProtocolSerialization(newConf, protocol);
   return SequenceFile.createWriter(fs, newConf, name, keyClass, valClass, bufferSize, replication, blockSize, compressionType, codec, progress, newMetadata);
  }
}
