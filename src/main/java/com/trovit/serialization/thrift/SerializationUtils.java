package com.trovit.serialization.thrift;

import java.util.Collection;
import java.util.regex.Pattern;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.serializer.Serialization;

public class SerializationUtils {

  private SerializationUtils() {}
  /**
   * Use this method to enable this serialization in Hadoop
   */
  public static void enableSerialization(Configuration conf, Class<? extends Serialization> clazz) {
    String serClass = clazz.getName();
    Collection<String> currentSers = conf.getStringCollection("io.serializations");

    if(currentSers.size() == 0) {
      conf.set("io.serializations", serClass);
      return;
    }

    // Check if it is already present
    if(!currentSers.contains(serClass)) {
      currentSers.add(serClass);
      conf.setStrings("io.serializations", currentSers.toArray(new String[] {}));
    }
  }

  /**
   * Use this method to disable this serialization in Hadoop
   */
  public static void disableSerialization(Configuration conf, Class<? extends Serialization> clazz) {
    String ser = conf.get("io.serializations").trim();
    String stToSearch = Pattern.quote("," + clazz.getName());
    ser = ser.replaceAll(stToSearch, "");
    conf.set("io.serializations", ser);
  }
  
  
}
