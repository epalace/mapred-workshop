package com.trovit.serialization.thrift;

import org.apache.hadoop.conf.Configurable;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.io.serializer.Serialization;
import org.apache.hadoop.io.serializer.Serializer;
import org.apache.thrift.TBase;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.transport.TTransport;

/**
 */
public class GeneralThriftSerialization implements Serialization<TBase>, Configurable {

  private Configuration conf;
  public GeneralThriftSerialization() {}
  
  private static final String CONF_THRIFT_PROTOCOL_SERIALIZATION_CLASS = GeneralThriftSerialization.class.getName()
      + "-thrift.serialization.protocol.class";
  public static final Class<? extends TProtocol> DEFAULT_THRIFT_SERIALIZATION_PROTOCOL = TBinaryProtocol.class;

  /**
   * PACKAGE VISIBILITY. INTENDED TO BE USED ONLY BY
   * {@link ThriftSequenceFileInputFormat} ,
   * {@link ThriftSequenceFileOutputFormat} or {@link ThriftSequenceFile}
   */
  public static Class<? extends TProtocol> getThriftProtocolSerialization(Configuration conf) {
    return conf.getClass(CONF_THRIFT_PROTOCOL_SERIALIZATION_CLASS, null, TProtocol.class);
  }

  /**
   * PACKAGE VISIBILITY. INTENDED TO BE USED ONLY BY
   * {@link ThriftSequenceFileInputFormat} ,
   * {@link ThriftSequenceFileOutputFormat} or {@link ThriftSequenceFile}
   */
  static void setThriftProtocolSerialization(Configuration conf, Class<? extends TProtocol> protocolClass) {
    conf.setClass(CONF_THRIFT_PROTOCOL_SERIALIZATION_CLASS, protocolClass, TProtocol.class);
    enableSerialization(conf);
  }

  public boolean accept(Class<?> c) {
    return TBase.class.isAssignableFrom(c);
  }

  public Deserializer<TBase> getDeserializer(Class<TBase> c) {
    Class<? extends TProtocol> protocol = getThriftProtocolSerialization(conf);
    return new ThriftDeserializer<TBase>(c, protocol);
  }

  public Serializer<TBase> getSerializer(Class<TBase> c) {
    Class<? extends TProtocol> protocolClass = getThriftProtocolSerialization(conf);
    if (protocolClass != null) {
      return new ThriftSerializer(protocolClass);
    } else {
      return new ThriftSerializer(DEFAULT_THRIFT_SERIALIZATION_PROTOCOL);
    }
  }

  /**
   * Enables Thrift Serialization support in Hadoop.
   */
  public static void enableSerialization(Configuration conf) {
    SerializationUtils.enableSerialization(conf, GeneralThriftSerialization.class);
  }

  /**
   * Enables Thrift Serialization support in Hadoop.
   */
  public static void disableSerialization(Configuration conf) {
    SerializationUtils.disableSerialization(conf, GeneralThriftSerialization.class);
  }

  @Override
  public void setConf(Configuration conf) {
    if (conf != null) {
      this.conf = conf;
    }
  }

  @Override
  public Configuration getConf() {
    return conf;
  }

  /**
   * Instances the right  given the class and transport
   */
  public static TProtocol newProtocol(Class<? extends TProtocol> protocolClass,final  TTransport transport) {
    if (protocolClass == null || protocolClass == TBinaryProtocol.class) {
      return new TBinaryProtocol.Factory().getProtocol(transport);
    } else if (protocolClass == TCompactProtocol.class) {
      return new TCompactProtocol.Factory().getProtocol(transport);
    } else if (protocolClass == TTupleProtocol.class) {
      return new TTupleProtocol.Factory().getProtocol(transport);
    } else {
      throw new IllegalStateException("Unknown protocol class " + protocolClass);
    }
  }

}