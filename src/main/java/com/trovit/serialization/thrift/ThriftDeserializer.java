package com.trovit.serialization.thrift;

import java.io.IOException;
import java.io.InputStream;

import org.apache.hadoop.io.serializer.Deserializer;
import org.apache.hadoop.util.ReflectionUtils;
import org.apache.thrift.TBase;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TIOStreamTransport;

/***
 *
 */
public class ThriftDeserializer<T extends TBase> implements Deserializer<T> {

  private Class<T> tClass;
  private TIOStreamTransport transport;
  private TProtocol protocol;
  private final Class<? extends TProtocol> protocolClazz;

  public ThriftDeserializer(Class<T> tBaseClass){
    this(tBaseClass, TBinaryProtocol.class);
  }
  
  public ThriftDeserializer(Class<T> tBaseClass, Class<? extends TProtocol> protocolClazz) {
    this.tClass = tBaseClass;
    this.protocolClazz = protocolClazz;
  }

  @Override
  public void open(InputStream in) {
    transport = new TIOStreamTransport(in);
    protocol = GeneralThriftSerialization.newProtocol(protocolClazz, transport);
  }

  @Override
  public T deserialize(T t) throws IOException {
    T object;
    if (t == null){
      object = newInstance();

    } else {
      t.clear();
      object = t;
    }

    try {
      object.read(protocol);

    } catch (TException e) {
      throw new IOException(e.toString(),e);
    }

    return object;
  }

  private T newInstance() {
    return (T) ReflectionUtils.newInstance(tClass, null);
  }

    @Override
  public void close() throws IOException {
    if (transport != null) {
      transport.close();
    }
  }
}

